#!/bin/sh

COMPRESSED_OVERRIDE='H4sIAAAAAAAAA8VRy04CMRTdz1c0bFhRBjTRNGGBYYILnEEQDTFkUpgLqUwfaTuj+PV2OiAQNxIX9iZ93HMfPeeWoA2TgqBmtxkEBnTJVmBIgBBVrDoQYpxugCAhMyA0V0yAd5cyL3gdWq0WauB2ldOWyrqLanhASW1PYzrdGxw665DrMAz9VgdmwhD0eou9LbxrJTmnIiNoR7VAxlJtvf9d6i0TmzRjmqBDO4+AKJmWgoOwh55xMojSKH4mSGmZ7Z2D6G42JKizf0Zx/24UpeNRfz6cJLN4cIQe5tPHUTpJkqd03J9OX5KJAxU1xn2irpbTJeSnFK2msGZbDIIuc+hZXUDjJ1oJ0zvSP8PWjoMFkWFduAL30ljilMWcfhagKXY61EkCbKVFJdw+teVIfuwWgYMPRS4e4nfi+ST/b0B/Ufjq1wq/UcbhEo2/ALOGA1k7AwAA'

set -xe

if [ -f docker-compose.override.yml ]; then
    mv docker-compose.override.y{,a}ml
fi

if [ ! -f docker-compose.override.yaml ]; then
    echo -n ${COMPRESSED_OVERRIDE} | base64 -d | gzip -d > docker-compose.override.yaml
fi

if ! docker network ls | awk '{print $2}' | grep 'traefik-proxy' &> /dev/null; then
    docker network create traefik-proxy;
fi

persistent_services=(
    traefik
    static
    db
)

for service in ${persistent_services[@]}; do
    if docker-compose ps | grep "${service}" | awk '{exit($3 == "Up")}'; then
        docker-compose up -d --build --force-recreate --remove-orphans "${service}"
    fi
done

docker-compose up -d --force-recreate --remove-orphans api frontend

set +x

echo 'Make sure you uncomment /etc/hosts'
echo 'Ready to start api and frontend'
