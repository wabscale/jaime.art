

create table if not exists jaime.`static`
(
    `path`    varchar(100) not null primary key,
    `pieceID` int unsigned not null,
    `type`    varchar(100) not null,

    FOREIGN KEY (pieceID) REFERENCES pieces(pieceID) ON DELETE CASCADE
) ENGINE=INNODB;


insert into static (`path`, `pieceID`, `type`)
select imgPath, pieceID, 'img' from jaime.pieces;

alter table jaime.pieces drop column imgPath;