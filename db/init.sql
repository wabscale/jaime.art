
create database if not exists jaime;
use jaime;

create table if not exists jaime.`pieces`
(
    `pieceID` int unsigned auto_increment primary key,
    `title`   varchar(100) null,
    `bio`     text         null,
    constraint pieces_title_unique unique (`title`)
) ENGINE=INNODB;

create table if not exists jaime.`config`
(
    `key`   varchar(100) not null primary key,
    `value` varchar(100) null
) ENGINE=INNODB;

create table if not exists jaime.`users`
(
    `id`       int unsigned auto_increment primary key,
    `username` varchar(100) null,
    `password` varchar(100) null,

    constraint users_username_unique unique (username)
) ENGINE=INNODB;


create table if not exists jaime.`static`
(
    `path`    varchar(100) not null primary key,
    `pieceID` int unsigned not null,
    `type`    varchar(100) not null,

    FOREIGN KEY (pieceID) REFERENCES pieces(pieceID) ON DELETE CASCADE
) ENGINE=INNODB;
