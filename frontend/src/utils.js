const path = require('path');

const getStaticPath = filePath => (
  filePath !== null ? 'https://' + path.join("static.mazuera.art", filePath) : null
);

export {
  getStaticPath,
};
