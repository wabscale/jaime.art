import withRoot from './withRoot';
// --- Post bootstrap -----
import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import ProductCategories from './views/Index/ProductCategories';
import AppFooter from './views/Index/AppFooter';
import ProductHero from './views/Index/ProductHero';
import ProductHowItWorks from './views/Index/ProductHowItWorks';
import AppAppBar from './views/Index/AppAppBar';
import Piece from "./views/Piece/Piece";
import Admin from './views/Admin/Admin';
import NotFound404 from './views/NotFound404';
import Gallery from "./views/Gallery/Gallery";

import ApolloClient from 'apollo-client';
import {ApolloProvider} from 'react-apollo';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {getStaticPath} from "./utils";

const {createUploadLink} = require('apollo-upload-client');
const {InMemoryCache} = require('apollo-cache-inmemory');


const authFetch = (uri, options) => {
  options.headers.token = localStorage.getItem('token');
  return fetch(uri, options);
};

const defaultOptions = {
  watchQuery: {
    fetchPolicy: 'network-only',
  },
  query: {
    fetchPolicy: 'no-cache',
  },
};

const API_HOST = window.location.host === 'localhost' ? 'http://localhost:4000/graphql' : 'https://api.mazuera.art/graphql';

const client = new ApolloClient({
  link: createUploadLink({
    uri: API_HOST,
    credentials: 'same-origin',
    fetch: authFetch,
  }),
  cache: new InMemoryCache(),
  defaultOptions: defaultOptions,
});


function Index() {
  return (
    <React.Fragment>
      <ProductHero/>
      <ProductCategories/>
      <ProductHowItWorks/>
      <AppFooter/>
    </React.Fragment>
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
  },
}));

function App() {
  const classes = useStyles();

  return (
    <ApolloProvider client={client}>
      <div className={classes.root}>
        <Router>
          <AppAppBar/>
          <Switch>
            <Route exact path="/" component={Index}/>
            <Route path="/admin" component={Admin}/>
            <Route path="/gallery" component={Gallery}/>
            <Route path="/piece/:pieceID" component={Piece}/>
            <Route component={NotFound404}/>
          </Switch>
        </Router>
      </div>
    </ApolloProvider>
  );
}

export default withRoot(App);
