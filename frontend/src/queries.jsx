import gql from "graphql-tag";

const GET_GRID_PHOTOS = gql`
  {
    getGridPieces {
      imgs {
        pieceID
        path
        type
      }
      pieceID
      title
    }
  }
`;

const GET_PIECE = gql`
  query getPiece ($pieceID: ID) {
    getPiece(pieceID: $pieceID) {
      pieceID
      title
      bio
      imgs {
        path
        type
        pieceID
      }
    }
  }
`;

const GET_DASHBOARD = gql`
  query getAdmin {
    whoAmI {
      id
      username
    }
    listUsers {
      id
      username
    }
    listPieces {
      imgs {
        pieceID
        path
        type
      }
      pieceID
      title
      bio
    }
  }
`;

const WHO_AM_I = gql`
  {
    whoAmI {
      id
      username
    }
  }
`;

const LOGIN = gql`
  query login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      token
    }
  }
`;

const YEET_PIECE = gql`
  mutation yeetPiece($pieceID: ID!) {
    yeetPiece(pieceID: $pieceID) {
      pieceID
    }
  }
`;


const YOINK_PIECE = gql`
  mutation yoinkPiece($pieceID: ID, $title: String!, $bio: String!, $img: [Upload]) {
    yoinkPiece(pieceID: $pieceID, title: $title, bio: $bio, imgs: $img) {
      pieceID
    }
  }
`;

const ADD_USER = gql`
  mutation addUser($username: String!, $password: String!) {
    addUser(username: $username, password: $password) {
      username
    }
  }
`;

const YEET_USER = gql`
  mutation yeetUser($username: String!) {
    yeetUser(username: $username) {
      username
    }
  }
`;

const GET_GALLERY = gql`
  {
    getGallery {
      imgs {
        pieceID
        path
        type
      }
      pieceID
      title
    }
  }
`;

const YOINK_STATIC = gql`
  mutation yoinkStatic($pieceID: ID, $content: Upload) {
    yoinkStatic(pieceID: $pieceID, content: $content) {
      pieceID
      path
      type
    } 
  }
`;

const YEET_STATIC = gql`
  mutation yeetStatic($pieceID: ID, $path: String) {
    yeetStatic(pieceID: $pieceID, path: $path) {
      pieceID
      path
      type
    }
  }
`;

const GET_STATIC = gql`
  query getStatic($pieceID: ID!) {
    getStatic(pieceID: $pieceID) {
      pieceID
      path
      type
    }
  }
`;

export {
  GET_GRID_PHOTOS,
  GET_PIECE,
  GET_DASHBOARD,
  LOGIN,
  WHO_AM_I,
  YEET_PIECE,
  YOINK_PIECE,
  ADD_USER,
  YEET_USER,
  GET_GALLERY,
  YEET_STATIC,
  GET_STATIC,
  YOINK_STATIC,
};
