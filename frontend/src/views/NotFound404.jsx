import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from "@material-ui/core/Grid";
import {getStaticPath} from "../utils";
import {Button} from "@material-ui/core";
import {Link} from "react-router-dom";
import clsx from "clsx";

const styles = theme => ({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(5),
    width: '100%',
    height: '100%'
  },
  background:{
    backgroundImage: `url(${getStaticPath('/static/productCurvyLines.png')})`,
    backgroundColor: theme.palette.secondary.light,
  },
  paper: {
    margin: 'auto',
    padding: theme.spacing(3, 3),
    color: theme.palette.text.secondary,
    width: '75%',
    maxWidth: 600,
  },
  img: {
    maxWidth: 500,
  },
});

class NotFound404 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {classes} = this.props;

    return (
      <div className={clsx(classes.root, classes.background)}>
        <Paper className={classes.paper}>
          <Grid
            container
            spacing={2}
            direction="column"
            alignItems="center"
            justify="center"
          >
            <Grid item xs key={"patrick"}>
              <img
                src={getStaticPath("static/404.png")}
                alt={"A quality meme was suppose to be here"}
                className={classes.img}
              />
            </Grid>
            <Grid item xs key={"404 message"}>
              <Typography
                className={classes.errorMessage}
                variant="h5"
              >
                Well this is embarrassing...<br/>
                I can't seem to find that 🤷
              </Typography>
            </Grid>
            <Grid item xs key={'go-home'}>
              <Button href="" to="/" color="secondary" component={Link}>
                Go Home
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </div>
    );
  }
};

NotFound404.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NotFound404);