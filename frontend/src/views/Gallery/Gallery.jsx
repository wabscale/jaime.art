import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import CircularProgress from "@material-ui/core/CircularProgress";
import {GET_GALLERY} from "../../queries";
import Query from "react-apollo/Query";
import {getStaticPath} from "../../utils";
import Typography from "../../modules/components/Typography";
import {Link as RouterLink} from "react-router-dom";
import ButtonBase from "@material-ui/core/ButtonBase";
import AppFooter from "../Index/AppFooter";
import withRoot from "../../withRoot";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    backgroundColor: theme.palette.secondary.light,
    // height: '100%',
    // backgroundColor: theme.palette.primary.light,
    backgroundImage: getStaticPath('/static/productCurvyLines.png'),
  },
  images: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexWrap: 'wrap',
  },
  imageWrapper: {
    position: 'relative',
    display: 'block',
    padding: 0,
    borderRadius: 0,
    height: '40vh',
    [theme.breakpoints.down('sm')]: {
      width: '100% !important',
      height: 100,
    },
    '&:hover': {
      zIndex: 1,
    },
    '&:hover $imageBackdrop': {
      opacity: 0.15,
    },
    '&:hover $imageMarked': {
      opacity: 0,
    },
    '&:hover $imageTitle': {
      border: '4px solid currentColor',
    },
  },
  imageButton: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.common.white,
  },
  imageSrc: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: 'cover',
    backgroundPosition: 'center 40%',
  },
  imageBackdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    background: theme.palette.common.black,
    opacity: 0.5,
    transition: theme.transitions.create('opacity'),
  },
  imageTitle: {
    position: 'relative',
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px 14px`,
  },
  imageMarked: {
    height: 3,
    width: 18,
    background: theme.palette.common.white,
    position: 'absolute',
    bottom: -2,
    left: 'calc(50% - 9px)',
    transition: theme.transitions.create('opacity'),
  },
  title: {
    paddingTop: theme.spacing(8),
  },
  progress: {
    margin: theme.spacing(2),
  },
}));
const WrappedFooter = withRoot(AppFooter);

function GridItem(props) {
  const classes = useStyles();
  const {piece} = props;
  const {pieceID, title, imgs} = piece;

  console.log(piece);

  return (
    <RouterLink
      className={classes.imageWrapper}
      style={{
        width: '33.3%',
      }}
      component={ButtonBase}
      to={`/piece/${pieceID}`}
    >
      <div
        className={classes.imageSrc}
        style={{
          backgroundImage: `url(${getStaticPath(imgs[0].path)})`,
        }}
      />
      <div className={classes.imageBackdrop}/>
      <div className={classes.imageButton}>
        <Typography
          component="h3"
          variant="h6"
          color="inherit"
          className={classes.imageTitle}
        >
          {title}
          <div className={classes.imageMarked}/>
        </Typography>
      </div>
    </RouterLink>
  );
}

function GalleryGrid(props) {
  const classes = useStyles();
  return (
      <section className={classes.root}>
        <Typography variant="h4" align="center" component="h2" className={classes.title}>
          Gallery
        </Typography>
        <div className={classes.images}>
          <Query query={GET_GALLERY}>
            {({loading, error, data}) => {
              if (loading) return <CircularProgress className={classes.progress}/>;
              if (error) return null;

              return data.getGallery.map((piece, index) => (
                <GridItem
                  piece={piece}
                  key={`piece-${index}`}
                />
              ));
            }}
          </Query>
        </div>
      </section>
  )
}

export default function Gallery() {
  return (
    <React.Fragment>
      <GalleryGrid/>
      <WrappedFooter/>
    </React.Fragment>
  );
};
