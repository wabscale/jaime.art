import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import {Query} from "react-apollo";
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import CircularProgress from "@material-ui/core/CircularProgress";
import ButtonBase from '@material-ui/core/ButtonBase';
import {withRouter} from "react-router-dom";
import {getStaticPath} from "../../utils";
import {GET_PIECE} from '../../queries';
import withRoot from '../../withRoot';
import Typography from "../../modules/components/Typography";
import AppFooter from "../Index/AppFooter";
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  root: {
    flex: 1,
    height: '100%',
    width: '100%',
    backgroundImage: `url(${getStaticPath('/static/productCurvyLines.png')})`,
    margin: theme.spacing(0),
  },
  errorMessage: {
    margin: theme.spacing(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    margin: theme.spacing(1),
    padding: theme.spacing(1),
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },
  typography: {
    // margin: theme.spacing(2)
  },
  paper: {
    // position: 'absolute',
    maxWidth: 500,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(4),
    outline: 'none',
    margin: 'auto',
    textAlign: 'center',
  },
  bioPaper: {
    width: '100%',
    height: '100%',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(4),
    outline: 'none',
    margin: 'auto',
  },
});

class Piece extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  redirect(path) {
    const {history} = this.props;
    history.push(path);
  }

  render() {
    const {classes, match} = this.props;
    const {pieceID} = match.params;

    return (
      <Fragment>
        <Grid
          container
          direction="row"
          justify="flex-start"
          spacing={2}
          className={classes.root}
          style={{
            height: '100%',
            width: '100%',
          }}
        >
          <Query query={GET_PIECE} variables={{pieceID}}>
            {({data, loading, error}) => {
              if (loading)
                return <CircularProgress className={classes.progress}/>;
              if (error) {
                setTimeout(() => this.redirect('/404'), 250);
                return null;
              }

              const {title, bio, imgs} = data.getPiece;

              // TODO
              // Make this actually display multiple images
              // on a gridlist or something
              const imgPath = imgs[0].path;

              return (
                <React.Fragment>
                  <Grid item md={6} sm={12} xs={12}>
                    <Paper className={classes.paper}>
                      <ButtonBase
                        className={classes.image}
                        href={getStaticPath(imgPath)}
                      >
                        <img
                          src={getStaticPath(imgPath)}
                          alt="hmmm... there should be something here"
                          className={classes.img}
                        />
                      </ButtonBase>
                    </Paper>
                  </Grid>
                  <Grid item xs>
                    <Paper className={classes.bioPaper}>
                      <Grid
                        container
                        spacing={2}
                        direction="column"
                      >
                        <Grid item xs>
                          <Typography
                            gutterBottom
                            variant="h4"
                            className={classes.typography}
                          >
                            {title}
                          </Typography>
                        </Grid>
                        <Grid item xs>
                          <Typography
                            gutterBottom
                            variant="body2"
                            className={classes.typography}
                          >
                            {bio}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Paper>
                  </Grid>
                </React.Fragment>
              );
            }}
          </Query>
        </Grid>
        <AppFooter/>
      </Fragment>
    );
  }
};

Piece.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withRoot(withStyles(styles)(Piece)));
