import React, {Component, Fragment} from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import withRoot from '../../withRoot';
import {Mutation} from 'react-apollo';
import Modal from "@material-ui/core/Modal";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/es/TextField/TextField";
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import IconButton from "@material-ui/core/IconButton";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import Link from "@material-ui/core/Link";
import {getStaticPath} from "../../utils";
import DeleteIcon from '@material-ui/icons/Delete';
import CircularProgress from "@material-ui/core/CircularProgress";
import Query from "react-apollo/Query";
import {
  YEET_STATIC,
  YOINK_PIECE,
  GET_STATIC,
  YOINK_STATIC,
} from '../../queries';


const styles = theme => ({
  root: {
    flex: 1,
    display: 'flex',
    flexWrap: 'wrap',
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    position: 'absolute',
    width: 500,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(4),
    outline: 'none',
    margin: 'auto',
    textAlign: 'center',
  },
});


function StaticItem(props) {
  const {pieceID, path, type, refetch, classes} = props;

  const wrapYeet = yeetStatic => () => {
    yeetStatic({variables: {pieceID, path}});
    refetch();
  };

  return (
    <Fragment>
      <ListItem button component="">
        <ListItemText
          primary={
            <Link color={"textSecondary"} href={getStaticPath(path)} component={Link}>
              {path}
            </Link>
          }
          secondary={`content type - ${type}`}
        />
        <Mutation mutation={YEET_STATIC}>
          {(yeetStatic) => (
            <ListItemIcon>
              <IconButton href="" onClick={wrapYeet(yeetStatic)}>
                <DeleteIcon/>
              </IconButton>
            </ListItemIcon>
          )}
        </Mutation>
      </ListItem>
      <Divider/>
    </Fragment>
  );
}

function StaticList(props) {
  const {classes, pieceID} = props;

  if (pieceID === null) {
    return null;
  }

  return (
    <List component="nav" aria-label="Main mailbox folders">
      <Query query={GET_STATIC} variables={{pieceID}} pollInterval={1000}>
        {({data, error, loading, refetch}) => {
          if (loading) return <CircularProgress className={classes.progress}/>;
          if (error) {
            return null;
          }

          const {getStatic} = data;

          if (getStatic === null)
            return <div/>;

          return data.getStatic.map((item, index) => (
            <StaticItem
              key={`static-item-${index}`}
              refetch={refetch}
              {...item}
            />
          ));
        }}
      </Query>
    </List>
  );
}


class EditPiece extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pieceID: null,
      title: '',
      bio: '',
      imgs: null,
    };
  }

  wrapClose = onClose => () => {
    this.setState({
      pieceID: null,
      title: '',
      bio: '',
      files: null,
    });
    onClose();
  };

  wrapStaticYoink = (yoinkStatic, pieceID) => ({target: {validity, files}}) => {
    if (validity.valid) {
      const imgs = Array(...files);
      this.setState({files: imgs});
      imgs.forEach(content => yoinkStatic({variables: {pieceID, content}}));
    }
  };

  handleYoink = (yoinkPiece, client) => async () => {
    const {pieceID} = this.state;

    const {data} = await yoinkPiece({
      variables: this.state,
    });

    if (pieceID === null) {
      this.state.files.forEach(content => client.mutate({
        mutation: YOINK_STATIC,
        variables: {
          pieceID: data.yoinkPiece.pieceID,
          content,
        }
      }))
    }

    debugger

    this.onClose();
  };

  onClose = this.wrapClose(this.props.onClose);

  render() {
    const {
      classes,
      forceUpdate,
      open = false,
      onRendered = function () {
      },
    } = this.props;

    const StaticListWrapped = withStyles(styles)(StaticList);

    return (
      <Modal open={!!open} onClose={this.onClose} className={classes.root} onRendered={onRendered.bind(this)}>
        <Mutation
          mutation={YOINK_PIECE}
          onCompleted={forceUpdate}
          onError={e => {
            alert(e);
          }}
        >
          {(yoinkPiece, data) => (
            <Paper className={classes.paper}>
              <Grid container spacing={2}>
                <Grid item key="title" xs={12}>
                  <Typography
                    variant="h5"
                    component="h3"
                  >
                    New / Edit Piece
                  </Typography>
                </Grid>
                <Grid item key="title-input" xs={12}>
                  <TextField
                    required
                    id="outlined-required"
                    name="title"
                    label="Title"
                    className={classes.textField}
                    margin="normal"
                    variant="outlined"
                    value={this.state.title}
                    onChange={e => this.setState({
                      title: e.target.value,
                    })}
                    // onKeyPress={e => e.key === 'Enter' ? this.login(client) : null}
                  />
                </Grid>
                <Grid item key="bio-input" xs={12}>
                  <TextField
                    required
                    id="outlined-password-input"
                    name="bio"
                    label="Bio"
                    className={classes.textField}
                    margin="normal"
                    variant="outlined"
                    value={this.state.bio}
                    onChange={e => this.setState({
                      bio: e.target.value,
                    })}
                    // onKeyPress={e => e.key === 'Enter' ? this.login(client) : null}
                    multiline
                    rowsMax="4"
                  />
                </Grid>
                <Grid item xs={12} key={`static-list`}>
                  <StaticListWrapped
                    pieceID={this.state.pieceID}
                  />
                </Grid>
                <Grid item xs={12} key={"img-input"}>
                  <Mutation
                    mutation={YOINK_STATIC}
                    onCompleted={forceUpdate}
                    onError={e => {
                      alert(e);
                    }}
                  >
                    {(yoinkStatic) => (
                      <Fragment>
                        <input
                          accept="*"
                          className={classes.input}
                          id="icon-button-file"
                          onChange={this.wrapStaticYoink(yoinkStatic, this.state.pieceID)}
                          type="file"
                          hidden
                        />
                        <label htmlFor="icon-button-file">
                          <IconButton variant="contained" className={classes.addUser} component="span" href="">
                            <CloudUploadIcon/>
                          </IconButton>
                        </label>
                      </Fragment>
                    )}
                  </Mutation>
                </Grid>
                <Grid item key="submit" xs={12}>
                  <Button
                    variant="contained"
                    className={classes.button}
                    onClick={this.handleYoink(yoinkPiece, data.client)}
                    color="primary"
                    type="submit"
                    href=""
                  >
                    Submit
                  </Button>
                </Grid>
              </Grid>
            </Paper>
          )}
        </Mutation>
      </Modal>
    );
  }
};

EditPiece.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRoot(withStyles(styles)(EditPiece));
