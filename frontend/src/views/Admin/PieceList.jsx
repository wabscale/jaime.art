import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {Mutation} from 'react-apollo';
import {Link as RouterLink} from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Link from '@material-ui/core/Link';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import {YEET_PIECE} from '../../queries';
import {getStaticPath} from "../../utils";

const styles = theme => ({
  root: {
    flex: true,
    padding: theme.spacing(3),
    backgroundImage: getStaticPath('/static/productCurvyLines.png')
  },
  paper: {
    padding: theme.spacing(3, 2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  smallPaper: {
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  right: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-end',
  },
});

function PieceList(props) {
  const {classes, pieces, activateEditPiece, activateSanityCheck} = props;
  return (
    <Paper className={classes.paper}>
      <Typography variant="h5" component="h3">
        Pieces
      </Typography>
      <List component="nav" aria-label="Main mailbox folders">
        <ListItem button key={"new-piece"} onClick={activateEditPiece()}>
          <ListItemText primary={"New Piece"}/>
          <ListItemIcon>
            <IconButton href="">
              <AddIcon/>
            </IconButton>
          </ListItemIcon>
        </ListItem>
        <Divider/>
        {pieces.map(({pieceID, title, imgs, bio}) => (
          <ListItem
            key={`piece-${pieceID}`}
            button
          >
            <ListItemText
              primary={
                <Link color={"textSecondary"} href={`/piece/${pieceID}`} component={Link}>
                  {title}
                </Link>
              }
              secondary={
                <Link color={"textSecondary"} href={getStaticPath(imgs[0].path)} component={Link}>
                  {imgs[0].path}
                </Link>
              }
              key={`piece-title-${pieceID}`}
            />
            <ListItemIcon>
              <IconButton href="" to={`/piece/${pieceID}`} component={RouterLink}>
                <OpenInNewIcon/>
              </IconButton>
            </ListItemIcon>
            <ListItemIcon
              onClick={activateEditPiece({
                onRendered() {
                  this.setState({
                    pieceID,
                    title,
                    bio,
                    imgs,
                  });
                }
              })}
            >
              <IconButton href="">
                <EditIcon/>
              </IconButton>
            </ListItemIcon>
            <Mutation mutation={YEET_PIECE} key={`piece-mutation-${pieceID}`}>
              {(yeetPiece) => (
                <ListItemIcon>
                  <IconButton href="" onClick={() => activateSanityCheck({
                    onYes: () => (
                      yeetPiece({variables: {pieceID}})
                    ),
                    message: `Are you sure you want to yeet ${title}?`
                  })}>
                    <DeleteIcon/>
                  </IconButton>
                </ListItemIcon>
              )}
            </Mutation>
          </ListItem>
        ))}
      </List>
    </Paper>
  );
}

PieceList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PieceList);
