import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import withRoot from '../../withRoot';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Modal from '@material-ui/core/Modal';
import Grid from "@material-ui/core/Grid";
import Button from '@material-ui/core/Button';
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
  root: {
    flex: 1,
    display: 'flex',
    flexWrap: 'wrap',
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    position: 'absolute',
    width: 500,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(4),
    outline: 'none',
    margin: 'auto',
    textAlign: 'center',
  },
  button: {
    margin: theme.spacing(1),
  }
});

function SanityCheck(props) {
  const {
    classes,
    open,
    onClose,
    message = 'Are you sure?',
    onNo = () => {},
    onYes,
  } = props;

  const wrapClose = func => args => {
    func(args);
    onClose();
  };

  return (
    <Modal open={open} onClose={onClose} className={classes.root}>
      <Paper className={classes.paper}>
        <Grid container spacing={8} direction={'row'}>
          <Grid item xs={12} key={'message'}>
            <Typography variant={'body1'} gutterBottom>
              {message}
            </Typography>
          </Grid>
          <Grid item xs key={'no'}>
            <Button
              variant="contained"
              className={classes.button}
              href={""}
              onClick={wrapClose(onNo)}
            >
              Nah Bruh
            </Button>
          </Grid>
          <Grid item xs key={'yes'}>
            <Button
              variant="contained"
              color={"primary"}
              className={classes.button}
              href={""}
              onClick={wrapClose(onYes)}
            >
              Damn Right
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </Modal>
  );
};

SanityCheck.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRoot(withStyles(styles)(SanityCheck));