import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {Query, withApollo} from 'react-apollo';
import Cookies from 'universal-cookie';
import {withRouter} from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import withRoot from '../../withRoot';
import {getStaticPath} from "../../utils";
import {GET_DASHBOARD,} from '../../queries';
import EditPiece from './EditPiece';
import SanityCheck from './SanityCheck';
import NewUser from './NewUser';
import UserList from './UserList';
import PieceList from './PieceList';

const styles = theme => ({
  root: {
    flex: true,
    padding: theme.spacing(3),
    backgroundImage: getStaticPath('/static/productCurvyLines.png')
  },
  paper: {
    padding: theme.spacing(3, 2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editPieceActive: false,
      newUserActive: false,
      sanityCheckActive: false,
      sanityCheckProps: null,
      editPieceProps: null,
    };
    this.authed = false;
  }

  logOut = () => {
    const {client} = this.props;
    const cookies = new Cookies();
    this.authed = false;
    cookies.remove('token');
    localStorage.removeItem('token');
    client.resetStore(); // yeet cache
    this.redirect('/admin/login');
  };

  redirect = path => {
    this.resetModals();
    const {history} = this.props;
    history.push(path);
  };

  resetModals = ({editPieceActive = false, newUserActive = false, sanityCheckActive} = {}) => (
    this.setState({
      editPieceActive,
      newUserActive,
      sanityCheckActive
    })
  );

  activateModal = state => (e = null) => {
    if (e)
      e.preventDefault();
    this.resetModals(state);
  };

  activateEditPiece = (editPieceProps = null) => () => (
    this.setState({editPieceProps}, () => {
      this.activateModal({editPieceActive: true})();
    })
  );
  activateNewUser = this.activateModal({newUserActive: true});
  activateSanityCheck = (props) => {
    this.setState({sanityCheckProps: props}, () => (
      this.resetModals({sanityCheckActive: true})
    ));
  };

  render() {
    const {redirect, activateSanityCheck, activateEditPiece} = this;
    const {classes} = this.props;
    const {
      editPieceActive,
      newUserActive,
      sanityCheckActive,
      sanityCheckProps,
      editPieceProps
    } = this.state;

    return (
      <div className={classes.root}>
        <Query
          query={GET_DASHBOARD}
          key={'dashboard-main'}
          pollInterval={1000}
          // fetchPolicy={'no-cache'}
        >
          {({data, loading, error}) => {
            if (loading) return <CircularProgress className={classes.progress}/>;
            this.authed = !!localStorage.getItem('token');
            if (error || !this.authed) {
              console.log(this.authed);
              setTimeout(() => redirect('/admin/login'), 500);
              return <div/>;
            }

            const {
              listPieces: pieces,
              listUsers: users
            } = data;

            return (
              <Grid
                container
                spacing={2}
                direction="row"
              >
                <Grid item xs key={"user-list"}>
                  <UserList
                    users={users}
                    logout={() => this.logOut()}
                    activateNewUser={() => this.activateNewUser()}
                  />
                </Grid>
                <Grid item xs key={"piece-list"}>
                  <PieceList
                    pieces={pieces}
                    activateEditPiece={activateEditPiece}
                    activateSanityCheck={activateSanityCheck}
                  />
                </Grid>
              </Grid>
            );
          }}
        </Query>
        <NewUser
          key={'new-user-modal'}
          open={newUserActive}
          onClose={this.resetModals}
          forceUpdate={() => this.forceUpdate()}
        />
        <EditPiece
          key={'new-piece-modal'}
          open={editPieceActive}
          onClose={this.resetModals}
          forceUpdate={() => this.forceUpdate()}
          {...editPieceProps}
        />
        <SanityCheck
          key={'sanity-check-modal'}
          open={sanityCheckActive}
          onClose={this.resetModals}
          {...sanityCheckProps}
        />
      </div>
    );
  }
};

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withApollo(withRouter(withRoot(withStyles(styles)(Dashboard))));
