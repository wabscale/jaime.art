import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {BrowserRouter as Router, Route} from "react-router-dom";
import withRoot from '../../withRoot';
import Login from './Login';
import Dashboard from './Dashboard';
import {getStaticPath} from "../../utils";

const styles = theme => ({
  root: {
    flex: true,
    height: '100%',
    backgroundImage: `url(${getStaticPath('/static/productCurvyLines.png')})`,
    backgroundColor: theme.palette.secondary.light,
  },
});

function Admin(props) {
  const {classes, match} = props;

  return (
    <div className={classes.root}>
      <Router>
        <Route exact path={match.path} component={Dashboard}/>
        <Route path={`${match.path}/login`} component={Login}/>
      </Router>
    </div>
  );
};

Admin.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRoot(withStyles(styles)(Admin));