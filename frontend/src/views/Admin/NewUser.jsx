import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import withRoot from '../../withRoot';
import {Mutation} from 'react-apollo';
import {ADD_USER} from '../../queries';
import Modal from "@material-ui/core/Modal";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/es/TextField/TextField";


const styles = theme => ({
  root: {
    flex: 1,
    display: 'flex',
    flexWrap: 'wrap',
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    position: 'absolute',
    width: 500,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(4),
    outline: 'none',
    margin: 'auto',
    textAlign: 'center',
  },
});

class NewUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  wrapClose = onClose => () => {
    this.setState({username:'',password:''});
    onClose();
  };

  onClose = this.wrapClose(this.props.onClose);

  render() {
    const {classes, open=false} = this.props;
    const {username, password} = this.state;

    return (
      <Modal open={!!open} onClose={this.onClose} className={classes.root}>
        <Mutation
          mutation={ADD_USER}
          onCompleted={this.props.forceUpdate}
          onError={e => alert(e)}
        >
          {(addUser) => (
            <Paper className={classes.paper}>
              <Grid container spacing={2}>
                <Grid item key="title" xs={12}>
                  <Typography
                    variant="h5"
                    component="h3"
                  >
                    New User
                  </Typography>
                </Grid>
                <Grid item key="username-input" xs={12}>
                  <TextField
                    required
                    id="outlined-required"
                    name="username"
                    label="Username"
                    className={classes.textField}
                    margin="normal"
                    variant="outlined"
                    value={username}
                    onChange={e => this.setState({
                      username: e.target.value,
                    })}
                    // onKeyPress={e => e.key === 'Enter' ? this.login(client) : null}
                  />
                </Grid>
                <Grid item key="password-input" xs={12}>
                  <TextField
                    required
                    id="outlined-password-input"
                    name="password"
                    label="Password"
                    className={classes.textField}
                    type="password"
                    margin="normal"
                    variant="outlined"
                    value={password}
                    onChange={e => this.setState({
                      password: e.target.value,
                    })}
                    // onKeyPress={e => e.key === 'Enter' ? this.login(client) : null}
                  />
                </Grid>
                <Grid item key="submit" xs={12}>
                  <Button
                    variant="contained"
                    className={classes.button}
                    onClick={() => {
                      addUser({variables: {username, password}});
                      this.onClose();
                    }}
                    color="primary"
                    type="submit"
                  >
                    Submit
                  </Button>
                </Grid>
              </Grid>
            </Paper>
          )}
        </Mutation>
      </Modal>
    );
  }
};

NewUser.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRoot(withStyles(styles)(NewUser));