import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {Mutation} from 'react-apollo';
import Paper from '@material-ui/core/Paper';
import {getStaticPath} from "../../utils";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSignOutAlt} from "@fortawesome/free-solid-svg-icons/faSignOutAlt";
import {YEET_USER,} from '../../queries';

const styles = theme => ({
  root: {
    flex: true,
    padding: theme.spacing(3),
    backgroundImage: getStaticPath('/static/productCurvyLines.png')
  },
  paper: {
    padding: theme.spacing(3, 2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  smallPaper: {
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  right: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-end',
  },
});

function UserList(props) {
  const {classes, users, logout, activateNewUser} = props;
  return (
    <Paper className={classes.paper}>
      <Typography variant="h5" component="h3">
        Users
      </Typography>
      <List component="nav" aria-label="Main mailbox folders">
        <ListItem button key={"sign-out"} onClick={logout}>
          <ListItemText primary={"Sign Out"}/>
          <ListItemIcon>
            <IconButton href="">
              <FontAwesomeIcon icon={faSignOutAlt}/>
            </IconButton>
          </ListItemIcon>
        </ListItem>
        <ListItem button key={"new-user"} onClick={activateNewUser}>
          <ListItemText primary={"New User"}/>
          <ListItemIcon>
            <IconButton href="">
              <AddIcon/>
            </IconButton>
          </ListItemIcon>
        </ListItem>
        <Divider/>
        {users.map(({id, username}) => (
          <Mutation mutation={YEET_USER} key={`user-yeet-${id}`}>
            {(yeetUser) => (
              <ListItem
                button
                key={`user-${id}`}
                onClick={() => yeetUser({variables: {username}})}
              >
                <ListItemText primary={username}/>
                {
                  ['admin','john'].includes(username) ? null :
                    <ListItemIcon>
                      <IconButton href="">
                        <DeleteIcon/>
                      </IconButton>
                    </ListItemIcon>
                }
              </ListItem>
            )}
          </Mutation>
        ))}
      </List>
    </Paper>
  );
}

UserList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserList);
