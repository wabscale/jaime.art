import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
// import Link from '@material-ui/core/Link';
import Container from '@material-ui/core/Container';
import Typography from '../../modules/components/Typography';
import TextField from '../../modules/components/TextField';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faFacebook, faInstagram} from '@fortawesome/free-brands-svg-icons';
// import compose from 'docs/src/modules/utils/compose';

const styles = theme => ({
  root: {
    // marginTop: theme.spacing(1),
    display: 'flex',
    backgroundColor: theme.palette.secondary.light,
    width: '100%'
  },
  container: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(8),
    display: 'flex',
  },
  iconsWrapper: {
    height: 120,
  },
  icons: {
    display: 'flex',
  },
  icon: {
    width: 48,
    height: 48,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.palette.warning.main,
    marginRight: theme.spacing(1),
    '&:hover': {
      backgroundColor: theme.palette.warning.dark,
    },
  },
  list: {
    margin: 0,
    listStyle: 'none',
    paddingLeft: 0,
  },
  listItem: {
    paddingTop: theme.spacing(0.5),
    paddingBottom: theme.spacing(0.5),
  },
  language: {
    marginTop: theme.spacing(1),
    width: 150,
  },
});

const LANGUAGES = [
  {
    code: 'en-US',
    name: 'English',
  },
  {
    code: 'es',
    name: 'Español',
  },
];

function AppFooter(props) {
  const {classes} = props;

  return (
    <Typography component="footer" className={classes.root}>
      <Container className={classes.container}>
        <Grid container spacing={5}>
          <Grid item xs={6} sm={4} md={3}>
            <Grid
              container
              direction="column"
              // justify="flex-end"
              // className={classes.iconsWrapper}
              spacing={4}
            >
              <Grid item xs={12}>
                <Typography variant="h5" component="h5">
                  Follow me on my journey through this world
                </Typography>
              </Grid>
              <Grid item className={classes.icons}>
                <a href="/" className={classes.icon}>
                  <FontAwesomeIcon icon={faFacebook}/>
                </a>
                <a href="/" className={classes.icon}>
                  <FontAwesomeIcon icon={faInstagram}/>
                </a>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={6} sm={4} md={2}>
          </Grid>
          <Grid item xs={6} sm={8} md={4}>
            <Typography variant="h6" marked="left" gutterBottom>
              Language
            </Typography>
            <TextField
              select
              SelectProps={{
                native: true,
              }}
              className={classes.language}
            >
              {LANGUAGES.map(language => (
                <option value={language.code} key={language.code}>
                  {language.name}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid item>
          </Grid>
        </Grid>
      </Container>
    </Typography>
  );
}

AppFooter.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AppFooter);
