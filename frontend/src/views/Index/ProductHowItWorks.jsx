import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Button from '../../modules/components/Button';
import Typography from '../../modules/components/Typography';
import {getStaticPath} from "../../utils";
import {Link} from 'react-router-dom';

const styles = theme => ({
  root: {
    display: 'flex',
    // backgroundColor: theme.palette.primary.light,
    overflow: 'hidden',
  },
  container: {
    marginTop: theme.spacing(10),
    marginBottom: theme.spacing(15),
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  item: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(0, 5),
  },
  title: {
    marginBottom: theme.spacing(14),
  },
  number: {
    fontSize: 24,
    fontFamily: theme.typography.fontFamily,
    color: theme.palette.secondary.main,
    fontWeight: theme.typography.fontWeightMedium,
  },
  image: {
    height: 55,
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  curvyLines: {
    pointerEvents: 'none',
    position: 'absolute',
    top: -180,
    opacity: 0.7,
  },
  button: {
    marginTop: theme.spacing(8),
  },
});

function ProductHowItWorks(props) {
  const {classes} = props;

  return (
    <section className={classes.root}>
      <Container className={classes.container}>
        <img
          src={getStaticPath('/static/productCurvyLines.png')}
          className={classes.curvyLines}
          alt="curvy lines"
        />
        <div className={classes.title}>
          <Typography variant="h4" marked="center" component="h2">
            My philosophies
          </Typography>
        </div>
        <div style={{width: '100%'}}>
          <Grid container spacing={5}>
            <Grid item xs={12} md={4}>
              <div className={classes.item}>
                {/*<div className={classes.number}>1.</div>*/}
                <Typography variant="h5" align="center">
                  Be Kind
                </Typography>
              </div>
            </Grid>
            <Grid item xs={12} md={4}>
              <div className={classes.item}>
                {/*<div className={classes.number}>2.</div>*/}
                <Typography variant="h5" align="center">
                  Be Cool
                </Typography>
              </div>
            </Grid>
            <Grid item xs={12} md={4}>
              <div className={classes.item}>
                {/*<div className={classes.number}>3.</div>*/}
                <Typography variant="h5" align="center">
                  {'Be Yourself'}
                </Typography>
              </div>
            </Grid>
          </Grid>
        </div>
        <Button
          color="secondary"
          size="large"
          variant="contained"
          className={classes.button}
          href=""
          to="/gallery"
          component={Link}
        >
          View Gallery
        </Button>
      </Container>
    </section>
  );
}

ProductHowItWorks.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductHowItWorks);
