import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import ButtonBase from '@material-ui/core/ButtonBase';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '../../modules/components/Typography';
import {Query} from "react-apollo";
import {GET_GRID_PHOTOS} from '../../queries';
import {getStaticPath} from '../../utils';
import {Link as RouterLink} from "react-router-dom";


const styles = theme => ({
  root: {
    // marginTop: theme.spacing(8),
    marginBottom: theme.spacing(4),
    backgroundColor: theme.palette.secondary.light,
    // height: '100%',
  },
  images: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexWrap: 'wrap',
  },
  imageWrapper: {
    position: 'relative',
    display: 'block',
    padding: 0,
    borderRadius: 0,
    height: '40vh',
    [theme.breakpoints.down('sm')]: {
      width: '100% !important',
      height: 100,
    },
    '&:hover': {
      zIndex: 1,
    },
    '&:hover $imageBackdrop': {
      opacity: 0.15,
    },
    '&:hover $imageMarked': {
      opacity: 0,
    },
    '&:hover $imageTitle': {
      border: '4px solid currentColor',
    },
  },
  imageButton: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.common.white,
  },
  imageSrc: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: 'cover',
    backgroundPosition: 'center 40%',
  },
  imageBackdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    background: theme.palette.common.black,
    opacity: 0.5,
    transition: theme.transitions.create('opacity'),
  },
  imageTitle: {
    position: 'relative',
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px 14px`,
  },
  imageMarked: {
    height: 3,
    width: 18,
    background: theme.palette.common.white,
    position: 'absolute',
    bottom: -2,
    left: 'calc(50% - 9px)',
    transition: theme.transitions.create('opacity'),
  },
  title: {
    paddingTop: theme.spacing(8),
  },
  progress: {
    margin: theme.spacing(2),
  },
});

function ProductCategories(props) {
  const {classes} = props;

  return (
    <section className={classes.root}>
      <Typography variant="h4" align="center" component="h2" className={classes.title}>
        For all tastes and all desires
      </Typography>
      <Typography variant="h5" align="center" component="h3" className={classes.title}>
        "Keep trying and it will pay off, trust me" <br/> - Jaime Mazuera 6.13.2018
      </Typography>
      <div className={classes.images}>
        <Query query={GET_GRID_PHOTOS}>
          {({loading, error, data}) => {
            if (loading) return <CircularProgress className={classes.progress}/>;
            if (error) return null;


            return data.getGridPieces.map(({pieceID, imgs, title = 'Word'}, index) => (
              <RouterLink
                key={index}
                className={classes.imageWrapper}
                style={{
                  width: '33.3%',
                }}
                component={ButtonBase}
                to={`/piece/${pieceID}`}
              >
                <div
                  className={classes.imageSrc}
                  style={{
                    backgroundImage: `url(${getStaticPath(imgs[0].path)})`,
                  }}
                />
                <div className={classes.imageBackdrop}/>
                <div className={classes.imageButton}>
                  <Typography
                    component="h3"
                    variant="h6"
                    color="inherit"
                    className={classes.imageTitle}
                  >
                    {title}
                    <div className={classes.imageMarked}/>
                  </Typography>
                </div>
              </RouterLink>
            ));
          }}
        </Query>
      </div>
    </section>
  );
}

ProductCategories.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductCategories);
