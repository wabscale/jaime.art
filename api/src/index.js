const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const session = require('koa-session');
const convert = require('koa-convert');
const helmet = require('koa-helmet');
const cors = require('@koa/cors');
const crypto = require('crypto');
const {ApolloServer} = require('apollo-server-koa');
const logger = require('koa-logger');

const {loadUser} = require('./auth');
const resolvers = require('./resolvers');
const typeDefs = require('./schema');

const app = new Koa();

app.keys = [
  crypto.randomBytes(32).toString('hex'),
  crypto.randomBytes(32).toString('hex')
];

if (process.env.DEBUG === '1') {
  app.use(logger());
}

app.use(loadUser);
app.use(convert(session(app)));
app.use(helmet());

// body parser
app.use(bodyParser({
  multipart: true,
  urlencode: true,
}));


app.use(cors({
  origin: '*',
}));

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ctx}) => ctx,
  playground: process.env.ENABLE_PLAYGROUND === '1'
});
server.applyMiddleware({app}); // /graphql

const router = new Router();

app.use(router.routes());
app.use(router.allowedMethods());

console.log("🚀 Server ready");
app.listen(4000, '0.0.0.0');
