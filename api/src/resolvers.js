// A map of functions which return data for the schema.

const queries = require('./queries');
const {loginUser} = require('./auth');
const {AuthenticationError} = require('apollo-server-koa');
const {
  handlePieceYeet,
  handlePieceYoink,
  handleStaticYoink
} = require('./files')

const requiresLogin = resolver => (parent, args, ctx, info) => {
  if (ctx.state.user)
    return resolver(parent, args, ctx, info);
  throw new AuthenticationError('Unauthorized');
};

const reduceUser = user => ({id: user.id, username: user.username});

const resolvers = {
  Query: {
    getPiece: (_, {pieceID}) => queries.getPiece(pieceID),
    getStatic: (_, {pieceID}) => queries.getStatic(pieceID),
    getGridPieces: queries.getGridPieces,
    login: (_, {username, password}) => loginUser(username, password),
    getGallery: queries.getGallery,

    // requires login
    whoAmI: requiresLogin(
      (_, __, ctx) => reduceUser(ctx.state.user)
    ),
    listUsers: requiresLogin(queries.listUsers),
    listPieces: requiresLogin(queries.listPieces),
  },
  Mutation: {
    // Users
    addUser: requiresLogin(
      (_, {username, password}) => queries.addUser(username, password)
    ),
    yeetUser: requiresLogin(
      (_, {username}, ctx) => (
        ctx.state.user.username !== username
          ? queries.yeetUser(username)
          : Error('You cant yeet yourself')
      )
    ),

    // Pieces
    yeetPiece: requiresLogin(
      (_, {pieceID}) => handlePieceYeet(pieceID)
    ),
    yoinkPiece: requiresLogin(
      (_, {pieceID, title, bio}) => handlePieceYoink(pieceID, title, bio)
    ),
    yeetStatic: requiresLogin(
      (_, {pieceID, path}) => queries.yeetStatic(pieceID, path)
    ),
    yoinkStatic: requiresLogin(
      (_, {pieceID, content}) => handleStaticYoink(pieceID, content)
    )
  }
};

module.exports = resolvers;
