const {gql} = require('apollo-server');


// The GraphQL schema
// language=GraphQL
const schema = gql`
  type Query {
    "A simple type for getting started!"
    getPiece(pieceID: ID): Piece!
    getStatic(pieceID: ID): [Static!]
    getGridPieces: [Piece]!
    whoAmI: User!
    listUsers: [User!]
    listPieces: [Piece!]
    login(username: String!, password: String!): AuthToken
    getGallery: [Piece!]
  }

  type Mutation {
    addUser(username: String!, password: String!): User!
    yeetUser(username: String!): User!
    yeetPiece(pieceID: ID!): Piece!
    yoinkStatic(pieceID: ID, content: Upload): Static
    yoinkPiece(pieceID: ID, title: String, bio: String, imgs: [Upload]): Piece!
    yeetStatic(pieceID: ID, path: String): Static
  }

  type AuthToken {
    token: String!
  }

  type User {
    id: ID
    username: String!
  }

  type Piece {
    pieceID: ID!
    title: String
    bio: String
    imgs: [Static]
  }

  type Static {
    pieceID: ID!
    path: String!
    type: String
  }
`;

module.exports = schema;
