const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const crypto = require("crypto");
const {AuthenticationError} = require('apollo-server-koa');
const Cookies = require('universal-cookie');

const {getConfig, getUserByUsername, getUserByID, setConfig} = require('./queries');

function comparePass(userPassword, databasePassword) {
  return bcrypt.compareSync(userPassword, databasePassword);
}

const generateKey = async () => {
  let key = crypto.randomBytes(32).toString('hex');
  await setConfig('siteKey', key);
  return key;
};

const loadKey = async () => {
  const key = await getConfig('siteKey');
  return key !== null ? key.value : await generateKey();
};

const loginUser = async (username, password) => {
  const user = await getUserByUsername(username);
  if (user === undefined || !comparePass(password, user.password))
    throw new AuthenticationError('Unauthorized');
  return {
    token: jwt.sign({id: user.id}, await loadKey(), {expiresIn: '7d'})
  };
};

const loadUser = async (ctx, next) => {
  const {cookies} = new Cookies(ctx.request.headers.cookie);
  try {
    const loadedToken = ctx.req.headers.token || cookies.token;
    const token = jwt.verify(loadedToken, await loadKey());
    ctx.state.user = await getUserByID(token.id);
  } catch(err) {}
  await next();
};

module.exports = {
  loadUser,
  loginUser
};