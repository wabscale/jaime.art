const fs = require('fs');
const crypto = require('crypto');
// const path = require('path');
const queries = require('./queries');

const UPLOAD_PATH = '/static';

const yeetPieceFile = async (filename) => {
  try {
    fs.unlinkSync(`${UPLOAD_PATH}/${filename}`);
  } catch (e) {
    console.log(e);
  }
};

const saveFile = (stream, filepath) => {
  return new Promise((resolve, reject) =>
    stream
      .on('error', error => {
        if (stream.truncated)
        // Delete the truncated file.
          fs.unlinkSync(filepath);
        reject(error);
      })
      .pipe(fs.createWriteStream(`${UPLOAD_PATH}/${filepath}`))
      .on('error', error => reject(error))
      .on('finish', () => resolve({filepath}))
  );
};

function checksumStream(stream) {
  return new Promise((resolve, reject) => {
    let hash = crypto.createHash('sha256');
    stream.on('error', err => reject(err));
    stream.on('data', chunk => hash.update(chunk));
    stream.on('end', () => resolve(hash.digest('hex')));
  });
}

const handleFileYoink = async (file, pieceID) => {
  //  re = /(?:\.([^.]+))?$/;
  const {createReadStream, mimetype} = await file;
  if (!['image/jpeg', 'image/png'].includes(mimetype)) {
    return Error('jpegs and png images only');
  }
  const hash = await checksumStream(createReadStream());
  const filepath = `/art/${hash.substr(0, 10)}.${
    mimetype === 'image/jpeg' ? 'jpg' : 'png'
  }`;

  const stream = createReadStream();
  await saveFile(stream, filepath);
  return filepath;
};

const handlePieceYeet = async (pieceID) => {
  const piece = await queries.getPiece(pieceID);
  await yeetPieceFile(piece.imgPath);
  await queries.yeetPiece(piece.pieceID);
};

const handlePieceYoink = async (pieceID, title, bio) => {
  if (!pieceID) {
    pieceID = (await queries.yoinkPiece(title, bio)).pieceID;
  }

  return {pieceID};
};

const handleStaticYoink = async (pieceID, content) => {
  const imgPath = await handleFileYoink(content, pieceID);
  await queries.addStatic(pieceID, imgPath, 'img');

  return {
    pieceID,
    path: imgPath,
    type: 'img',
  };
};

module.exports = {
  handlePieceYeet,
  handlePieceYoink,
  handleStaticYoink,
};
