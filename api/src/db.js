const Knex = require('knex');

const knex = Knex({
  client: 'mysql',
  connection: {
    host: process.env.NODE_ENV === 'prod' ? 'db' : '127.0.0.1',
    user: 'root',
    password: 'password',
    database: 'jaime'
  }
});

knex.schema.hasTable('pieces').then(function (exists) {
  if (!exists) {
    return knex.schema.createTable('pieces', function (t) {
      t.increments('pieceID').primary();
      t.string('title', 100).unique();
      t.text('bio');
    });
  }
});

knex.schema.hasTable('static').then(exists => {
  if (!exists) {
    return knex.schema.createTable('static', t => {
      t.integer('pieceID').notNullable();
      t.string('path', 100).primary();
      t.string('type', 100);

      t.foreign('pieceID').references('pieceID').inTable('pieces');
    });
  }
});

knex.schema.hasTable('users').then(function (exists) {
  if (!exists) {
    return knex.schema.createTable('users', function (t) {
      t.increments('id').primary();
      t.string('username', 100).unique();
      t.string('password', 100);
    });
  }
});

knex.schema.hasTable('config').then(function (exists) {
  if (!exists) {
    return knex.schema.createTable('config', function (t) {
      t.string('key', 100).primary();
      t.string('value', 100);
    });
  }
});

module.exports = knex;
