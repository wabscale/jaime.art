const knex = require('./db');
const bcrypt = require('bcryptjs');

module.exports = {
  knex,

  // Static
  getStatic: async pieceID => (
    await knex
      .select()
      .from('static')
      .where({pieceID})
  ),
  addStatic: async (pieceID, path, type) => (
    await knex('static')
      .insert({pieceID, path, type})
  ),

  /*
  TODO:
  it would be nice to be able to easily delete static.
   */
  yeetStatic: async (pieceID, path) => {
    // wrecking ball
    await knex('static')
      .where({
        pieceID,
        path,
      })
      .del()
  },

  // Pieces
  listPieces: async () => {
    const pieces = await knex
      .select('*')
      .from('pieces');

    for (i=0; i<pieces.length; ++i) {
      let {pieceID} = pieces[i];
      pieces[i].imgs = await knex.select().from('static').where({pieceID});
    }

    return pieces;
  },
  getPiece: async pieceID => {
    const piece = (await knex
        .select('pieceID')
        .from('pieces')
        .where({pieceID})
    )[0];
    piece.imgs = await knex.select().from('static').where({pieceID});
    return piece;
  },
  updatePiece: async (pieceID, title, bio) => (
    knex('pieces').update({
      title,
      bio,
    }).where({
      pieceID
    })
  ),
  yoinkPiece: async (title, bio) => {
    await knex('pieces')
      .insert({title, bio});
    const piece = await knex
      .select('*')
      .from('pieces')
      .where({title});
    return piece.length === 1 ? piece[0] : null;
  },
  yeetPiece: async pieceID => {
    // yeet dependent rows
    // (there is an on delete cascade but just in case)
    await knex('static')
      .where({pieceID})
      .del();

    // yeet piece title and bio
    await knex('pieces')
      .where({pieceID})
      .del();
  },
  getGridPieces: async (count = 9) => {
    const pieces = await knex
      .select('*')
      .from('pieces')
      .limit(count);

    for (i=0; i<pieces.length; ++i) {
      let {pieceID} = pieces[i];
      pieces[i].imgs = await knex.select().from('static').where({pieceID});
    }

    return pieces;
  },
  getGallery: async () => {
    const pieces = await knex
      .select('*')
      .from('pieces');

    for (i=0; i<pieces.length; ++i) {
      let {pieceID} = pieces[i];
      pieces[i].imgs = await knex.select().from('static').where({pieceID});
    }

    return pieces;
  },

  // Config
  getConfig: async key => {
    const conf = (await knex
      .select('value')
      .from('config')
      .where({key}));
    return conf.length === 1 ? conf[0] : null;
  },
  setConfig: async (key, value) => (
    knex('config')
      .insert({key, value})
  ),

  // Users
  listUsers: async () => (
    knex
      .select('id', 'username')
      .from('users')
  ),
  getUserByUsername: async username => (
    (await knex
      .select('*')
      .from('users')
      .where({username}))[0]
  ),
  getUserByID: async id => (
    (await knex
      .select('*')
      .from('users')
      .where({id}))[0]
  ),
  addUser: async (username, password) => {
    await knex('users')
      .insert({
        username,
        password: bcrypt.hashSync(password, 10)
      });
    return (await knex
      .select('id', 'username')
      .from('users')
      .where({username}))[0];
  },
  yeetUser: async username => {
    if (username !== 'admin') {
      await knex('users')
        .where({username})
        .del();
      return username;
    }
    return Error('Can\'t yeet admin');
  },
};
