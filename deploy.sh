#!/bin/sh

cd $(dirname $(realpath $0))

export ACME_PATH=/var/lib/jaime/acme.json
export MYSQL_PATH=/var/lib/jaime/mysql
export STATIC_PATH=/var/lib/jaime/static

set -ex

if [ ! -f ${ACME_PATH} ]; then
    mkdir -p `echo ${ACME_PATH} | xargs dirname`
    touch ${ACME_PATH}
    chmod 600 ${ACME_PATH}
fi

if [ ! -d ${MYSQL_PATH} ]; then
    mkdir -p ${MYSQL_PATH}
    # chown -R gitlab-runner ${MYSQL_PATH}
fi

if ! docker network ls | grep "traefik-proxy"; then
    docker network create traefik-proxy
fi


docker-compose build

# if docker-compose ps traefik | grep 'traefik' | awk '{exit($3 != "Up")}'; then
#     docker-compose up -d --force-recreate traefik
# fi

docker-compose up --force-recreate --remove-orphans -d \
               frontend \
               api \
               redirect \
               static

# db  \
